package com.epam.ktorgenerator

import com.epam.generator.ApiCall
import com.epam.generator.KtorImpl
import com.epam.generator.KtorPath

data class Token(val value: String)

@KtorImpl
interface AccountApi {

    @KtorPath("accounts/login")
    fun login(user: String, password: String) : ApiCall<Token>

    @KtorPath("accounts/login2")
    suspend fun login2(user: String, password: String) : Token
}
