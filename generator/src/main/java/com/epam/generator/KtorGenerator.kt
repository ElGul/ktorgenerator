package com.epam.generator

import com.squareup.kotlinpoet.*
import java.io.File
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind
import javax.lang.model.element.TypeElement

class KtorGenerator : AbstractProcessor() {

    override fun getSupportedSourceVersion(): SourceVersion {
        return SourceVersion.latest()
    }

    override fun getSupportedAnnotationTypes(): MutableSet<String> {
        return mutableSetOf(KtorImpl::class.java.name)
    }

    override fun process(set: MutableSet<out TypeElement>?, roundEnv: RoundEnvironment): Boolean {
        println("process")
        roundEnv.getElementsAnnotatedWith(KtorImpl::class.java)
            .forEach {
                val pack = processingEnv.elementUtils.getPackageOf(it).toString()
                generateClass(it, pack)
            }
        return true
    }

    private fun generateClass(element: Element, pack: String) {

        val interfaceName = element.simpleName.toString()
        assert(element.kind == ElementKind.INTERFACE)

        val fileName = "${interfaceName}KtorImpl"

        val constructor = FunSpec.constructorBuilder()
            .addParameter("client", String::class)
            .build()

        val file = FileSpec.builder(pack, fileName)
            .addType(
                TypeSpec.classBuilder(fileName)
                    .addSuperinterface(ClassName(pack, interfaceName))
                    .primaryConstructor(constructor)
                    .addProperty(
                        PropertySpec.builder("client", String::class)
                        .initializer("client")
                        .addModifiers(KModifier.PRIVATE)
                        .build())
                    .apply {

                        element.enclosedElements.forEach {funElement ->
                            assert(funElement.kind ==  ElementKind.METHOD)

                            addFunction(
                                FunSpec.builder(funElement.simpleName.toString())
                                    .addModifiers(KModifier.OVERRIDE)
                                    .addStatement("TODO(\"Implement\")")
                                    .build()
                            )

                        }

                    }
                    .build())
            .build()

        val kaptKotlinGeneratedDir = processingEnv.options[KAPT_KOTLIN_GENERATED_OPTION_NAME]
        file.writeTo(File(kaptKotlinGeneratedDir, "$fileName.kt"))
    }

    companion object {
        const val KAPT_KOTLIN_GENERATED_OPTION_NAME = "kapt.kotlin.generated"
    }
}
