package com.epam.generator


class ApiCall<T>(private val call : suspend () -> T) {

    suspend fun await() = call()
}
