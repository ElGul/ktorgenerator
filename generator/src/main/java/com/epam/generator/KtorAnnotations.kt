package com.epam.generator

@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.CLASS)
annotation class KtorImpl

@Retention(AnnotationRetention.SOURCE)
annotation class KtorPath(val value: String)
